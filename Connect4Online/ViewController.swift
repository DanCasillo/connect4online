//
//  ViewController.swift
//  Connect4Online
//
//  Created by ArCaDi on 28/03/2020.
//  Copyright © 2020 ArCaDi. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var turnLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var game : Game!
    
    var name : String!
    var hosting : Bool = false
    
    
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var mcAdvertiserAssistant : MCAdvertiserAssistant!
    var messageToSend: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.game  = Game()
        game.delegate = self
        collectionView.isUserInteractionEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        
        multipeerSetup()
        
    }
    
    
    
    func reloadCollection(){
        collectionView.reloadData()
    }
    
    func reloadCell(at indexPath: IndexPath){
        //        self.counterInserted(at: indexPath, player: game.player)
        collectionView.reloadItems(at: [indexPath])
    }
    
    func prepareForPlay(){
        startButton.isEnabled = !startButton.isEnabled
        collectionView.isUserInteractionEnabled = !collectionView.isUserInteractionEnabled
    }
    
    @IBAction func startButton(_ sender: Any) {
        game.startGame()
        
    }
    
    @IBAction func actionButton(_ sender: Any) {
        let ac = UIAlertController(title: "Connection Menu", message: nil, preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: "Host a session", style: .default, handler: hostSession))
        ac.addAction(UIAlertAction(title: "Join a session", style: .default, handler: joinSession))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(ac, animated: true)
    }
    
}

extension ViewController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 7
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GameCVCell
        
        let positionStatus = game.board.checkStatus(at: indexPath)
        
        switch positionStatus {
        case .vuota:
            cell.imageView.image = #imageLiteral(resourceName: "vuoto")
        case .red:
            cell.imageView.image = #imageLiteral(resourceName: "rosso")
        case .yellow:
            cell.imageView.image = #imageLiteral(resourceName: "giallo")
        case .gold:
            if(game.player == .red){
                
                cell.imageView.image = #imageLiteral(resourceName: "rosso win")
            }
            else
            {
                cell.imageView.image = #imageLiteral(resourceName: "giallo win")
            }
        }
        
        cell.isUserInteractionEnabled = !(game.board.checkForCell(at: indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        game.move(at: indexPath)
        
    }
    
}

extension ViewController : MCSessionDelegate, MCBrowserViewControllerDelegate {
    
    // 1
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected: \(peerID.displayName)")
            DispatchQueue.main.async {
                if(self.hosting == true){
                    self.startButton.isHidden = false
                }
                self.turnLabel.text = "Your enemy is: \(peerID.displayName)"
            }
            
        case .connecting:
            print("Connecting: \(peerID.displayName)")
        case .notConnected:
            print("Not Connected: \(peerID.displayName)")
        @unknown default:
            print("fatal error")
        }
    }
    
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async {
            do{
                let index = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! IndexPath
                print("\(index) indice")
                self.game.move(at: index)
                
            } catch let error as NSError{
                print("Error is \(error)")
            }
            
        }
    }
    
    func counterInserted(at indexPath: IndexPath) {
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: indexPath, requiringSecureCoding: true)
            
            try mcSession.send(data, toPeers: mcSession.connectedPeers, with: .unreliable)
        } catch let error as NSError{
            print("error is \(error)")
        }
        
    }
    
    
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    
    func multipeerSetup(){
        peerID = MCPeerID(displayName: self.name)
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
        
    }
    
    func hostSession(action: UIAlertAction) {
        
        game.player = .red
        game.enemy = .yellow
        self.hosting = true
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "connect4game", discoveryInfo: nil, session: mcSession)
        mcAdvertiserAssistant.start()
        self.turnLabel.text = "Waiting for an enemy..."
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func joinSession(action: UIAlertAction) {
        game.player = .yellow
        game.enemy = .red
        self.hosting = false
        let mcBrowser = MCBrowserViewController(serviceType: "connect4game", session: mcSession)
        mcBrowser.delegate = self
        present(mcBrowser, animated: true)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
}






