//
//  GameClass.swift
//  Connect4Online
//
//  Created by ArCaDi on 28/03/2020.
//  Copyright © 2020 ArCaDi. All rights reserved.
//

import UIKit

class Game{
    enum directions : Int{
        case nordEst = 0
        case est
        case sudEst
        case sud
        case sudOvest
        case ovest
        case nordOvest
    }
    
    var delegate: ViewController!
    let offset : [Posizione]
    var player : Colore = .red
    var enemy : Colore = .yellow
    var turni : Int = 0
    var board : Griglia
    
    init(){
        offset = [Posizione(colonna: 1, riga: 1),Posizione(colonna: 1, riga: 0),
                  Posizione(colonna:1, riga: -1), Posizione(colonna: 0, riga: -1), Posizione(colonna: -1, riga: -1),Posizione(colonna: -1, riga: 0), Posizione(colonna: -1, riga: 1)]
        
        board = Griglia()
    }
    
    func startGame(){
        board = Griglia()
        if( delegate.hosting )
        {
            delegate.turnLabel.text = "Your turn"
            
        }
        
        delegate.reloadCollection()
        delegate.prepareForPlay()
    }
    
    func move(at indexPath: IndexPath){
        turni += 1
        
        if (delegate.collectionView.isUserInteractionEnabled == false){
            
            /*Creo pedina*/
            let pedina = Pedina(colour: enemy)
            /*Creo posizione*/
            let newPosition = getRow(at: indexPath)
            print("new : \(newPosition)")
            /*Inserisco*/
            insert(pedina: pedina, at: newPosition)
            /*Verifico vittoria*/
            if checkForWin(from: newPosition, pedina: pedina.colour){
                delegate.turnLabel.text = "Your Enemy Win!"
                turni = 0
                
            }
            else if turni >= 42 {
                delegate.turnLabel.text = "Draw!"
                turni = 0
                delegate.prepareForPlay()
            }
            else {
                
                delegate.turnLabel.text = "Your turn"
                delegate.collectionView.isUserInteractionEnabled = true
            }
            
        }
        else
        {
            /*Creo pedina*/
            let pedina = Pedina(colour: player)
            
            /*Creo posizione*/
            let newPosition = getRow(at: indexPath)
            print("new : \(newPosition)")
            
            /*Inserisco*/
            insert(pedina: pedina, at: newPosition)
            
            /*Verifico vittoria*/
            if checkForWin(from: newPosition, pedina: pedina.colour){
                delegate.turnLabel.text = "You Win!"
                turni = 0
                delegate.prepareForPlay()
            }
            else if turni >= 42 {
                delegate.turnLabel.text = "Draw!"
                turni = 0
                delegate.prepareForPlay()
            } else {
                
                delegate.turnLabel.text = "Enemy's turn"
                delegate.collectionView.isUserInteractionEnabled = false
                
            }
            
            delegate.counterInserted(at: indexPath)
            
        }
        
    }
    
    func insert(pedina: Pedina, at posizione: Posizione){
        board.colonne[posizione.colonna].add(pedina: pedina)
        delegate.reloadCell(at: PosizioneNormalizzata.toIndex(for: posizione))
    }
    
    func checkForWin(from position: Posizione, pedina: Colore) -> Bool{
        
        var win : Bool = false
        
        let vertical = contaConsecutivi(colour: pedina, from: position, to: offset[directions.sud.rawValue])
        
        if vertical >= 3 {
            print("vertical")
            win = true
        }
        
        let horizontal = contaConsecutivi(colour: pedina, from: position, to: offset[directions.est.rawValue]) + contaConsecutivi(colour: pedina, from: position, to: offset[directions.ovest.rawValue])
        
        if (horizontal >= 3)
        {
            print("horizontal")
            win = true
        }
        
        let rising = contaConsecutivi(colour: pedina, from: position, to: offset[directions.sudOvest.rawValue]) + contaConsecutivi(colour: pedina, from: position, to: offset[directions.nordEst.rawValue])
        
        if rising >= 3
        {
            print("rising")
            win = true
        }
        
        let falling = contaConsecutivi(colour: pedina, from: position, to: offset[directions.nordOvest.rawValue]) + contaConsecutivi(colour: pedina, from: position, to: offset[directions.sudEst.rawValue])
        
        if falling >= 3
        {
            print("Falling")
            win = true
        }
        
        return win
    }
    
    func contaConsecutivi(colour: Colore,from position: Posizione,to offset: Posizione)->Int{
        
        let statoCella : StatoCella = colour == .red ? .red : .yellow
        
        guard let newPosition = generatePosition(from: position, to: offset) else {
            return 0
        }
        
        guard board.checkForCell(at: PosizioneNormalizzata.toIndex(for: newPosition)) else {return 0}
        
        guard board.checkStatus(at: PosizioneNormalizzata.toIndex(for: newPosition)) == statoCella else {return 0}
        
        return 1 + contaConsecutivi(colour: colour, from: newPosition, to: offset)
    }
    
    
    
    func generatePosition(from posizione: Posizione, to offset : Posizione) -> Posizione?{
        let newPosition = Posizione(colonna: posizione.colonna + offset.colonna, riga: posizione.riga + offset.riga)
        print(newPosition)
        if (isValidPosition(newPosition))
        {
            return nil
        }
        else {
            return newPosition
        }
    }
    
    func isValidPosition(_ position: Posizione) -> Bool {
        return position.colonna < 0 || position.colonna > 6  || position.riga < 0 || position.riga > 5
    }
    /* func handleWin(pedina: Pedina){
     if(pedina.colour == .red)
     {
     print("You won!!")
     }
     else {
     print("Enemy won!!")
     }
     }*/
    
    
    func getRow(at indexPath: IndexPath) -> Posizione{
        
        let position = PosizioneNormalizzata.toModel(for: indexPath)
        let column = (position.colonna)
        let newPosition = Posizione(colonna: column, riga:board.colonne[column].pedine.count)
        return newPosition
        
    }
}
