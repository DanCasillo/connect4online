//
//  InitialViewController.swift
//  Connect4Online
//
//  Created by ArCaDi on 28/03/2020.
//  Copyright © 2020 ArCaDi. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameField.delegate = self

    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        self.performSegue(withIdentifier: "nextController", sender: self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text!.count > 0)
        {
            nextButton.isEnabled = true
        }
        else {
            nextButton.isEnabled = false
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nextController"{
            let vc = segue.destination as? ViewController
            
            vc?.name = self.nameField.text 
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
