//
//  Models.swift
//  Connect4Online
//
//  Created by ArCaDi on 28/03/2020.
//  Copyright © 2020 ArCaDi. All rights reserved.
//

import UIKit

let maxPerColonna = 6
enum Colore : Int {
    case gold = 0
    case red
    case yellow
}

enum StatoCella{
    case vuota
    case gold
    case red
    case yellow
}

struct Pedina {
    var colour : Colore
}

struct Colonna {
    var pedine = [Pedina]()
    
    mutating func add(pedina : Pedina)
    {
        guard pedine.count < maxPerColonna else { return }
        pedine.append(pedina)
    }
}

struct Posizione {
    var colonna: Int
    var riga: Int
}

class Griglia {
    var colonne = [Colonna]()
    
    init(){
        for _ in 1...7 {
            colonne.append(Colonna())
        }
    }
    
    func checkForCell(at indexPath: IndexPath) -> Bool {
        let pos = PosizioneNormalizzata.toModel(for: indexPath )
        
        return self.colonne[pos.colonna].pedine.count > (pos.riga)
    }
    
    func getPedina(at indexPath: IndexPath) -> Pedina {
        let pos = PosizioneNormalizzata.toModel(for: indexPath)
        print("pos :\(pos)")
        return colonne[pos.colonna].pedine[pos.riga]
    }
    
    func checkStatus(at indexPath : IndexPath) -> StatoCella {
        guard (checkForCell(at: indexPath)) else {return .vuota}
        let pedina = self.getPedina(at: indexPath)
        switch pedina.colour {
        case .red:
            return .red
        case .gold:
            return .gold
        case .yellow:
            return .yellow
        default:
            return .vuota
        }
        
    }
    
    
}
