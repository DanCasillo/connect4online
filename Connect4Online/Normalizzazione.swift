//
//  Normalizzazione.swift
//  Connect4Online
//
//  Created by ArCaDi on 28/03/2020.
//  Copyright © 2020 ArCaDi. All rights reserved.
//

import Foundation

class PosizioneNormalizzata {

    class func toModel(for indexPath: IndexPath) -> Posizione {
       return Posizione(colonna:indexPath.item, riga: maxPerColonna - 1 - indexPath.section)
    }
    
    class func toIndex(for pos:Posizione)-> IndexPath{
        return IndexPath(item: pos.colonna, section: maxPerColonna - 1 - pos.riga)
    }
}
